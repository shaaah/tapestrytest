package com.Repertoire.repertoireTest.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.tapestry5.beaneditor.NonVisual;
import org.apache.tapestry5.beaneditor.Validate;

@Entity
public class EntreeRepertoire {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonVisual
	public Long id;

	@Validate("required")
	public String nomComplet;
	
	public String tel;
	
	@Validate("required")
	public String rue;
	
	@Validate("required")
	public String codePostal;
	
	@Validate("required")
	public String ville;
	
	@Validate("required")
	public String pays;
}
