package com.Repertoire.repertoireTest.pages;

import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;

import com.Repertoire.repertoireTest.entities.EntreeRepertoire;


/**
 * Start page of application repertoireTest.
 */
public class Index
{
	@Inject
    private Session session;
	
	@Property
	private EntreeRepertoire entre;
	
    public List<EntreeRepertoire> getEntrees()
    {
        return session.createCriteria(EntreeRepertoire.class).list();
    }
}
