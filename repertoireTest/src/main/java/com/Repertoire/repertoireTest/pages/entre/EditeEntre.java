package com.Repertoire.repertoireTest.pages.entre;

import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.alerts.AlertManager;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.hibernate.Session;

import com.Repertoire.repertoireTest.entities.EntreeRepertoire;

public class EditeEntre {

	@Persist
	@Property
	private EntreeRepertoire entre;
	
	@Inject
	private Session session;
	
	@Inject
	private PageRenderLinkSource pageRenderLS;

	@Inject
	private AlertManager alertManager;
	
	void onActivate(EventContext eContext)
	{
		if (eContext.getCount() != 1)
		{
			alertManager.info("Nombre de paramètres incorrect.");
		}
		else{
			try {
				long entreId = eContext.get(long.class, 0);
				entre = (EntreeRepertoire) session.get(EntreeRepertoire.class, entreId);
				if(entre == null)
				{
					entre = new EntreeRepertoire();
					alertManager.info("Cette entrée n'existe pas!");
				}
			} catch (RuntimeException e) {
				entre = new EntreeRepertoire();
				alertManager.info("Type du paramètre incorrect.");
			}
		}
	}
	
	@CommitAfter
	Object onSuccess()
	{
		session.saveOrUpdate(entre);
		return pageRenderLS.createPageRenderLinkWithContext("entre/voir",entre.id);
	}
	
}
