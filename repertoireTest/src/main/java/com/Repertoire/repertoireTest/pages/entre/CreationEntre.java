package com.Repertoire.repertoireTest.pages.entre;

import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;

import com.Repertoire.repertoireTest.entities.EntreeRepertoire;
import com.Repertoire.repertoireTest.pages.Index;

public class CreationEntre {
	
	@Property
	private EntreeRepertoire entre;
	
	@Inject
	private Session session;
	
	@InjectPage
	private Index index;
	
	@CommitAfter
	Object onSuccess()
	{
		session.persist(entre);
		return index;
	}
	
}
